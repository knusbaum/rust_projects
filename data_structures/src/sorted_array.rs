use std::cmp::PartialOrd;
use std::fmt::Debug;

//fn parent_index(i: usize) -> usize {
//    return (i - 1) / 2;
//}

fn left_child(i: usize) -> usize {
    return (2 * i) + 1;
}

fn right_child(i: usize) -> usize {
    return (2 * i) + 2;
}

pub fn heapify<T>(a: &mut [T], root: usize)
        where T: PartialOrd + Debug
{
    //println!("heapify({:?}, {})", a, root);
    println!("heapify ({}): {:?}", root, a);

    let left_index = left_child(root);
    let right_index = right_child(root);

    if left_index < a.len() {
        heapify(a, left_index);

        if a[left_index] > a[root] {
            a.swap(left_index, root);
            sift_up(a, left_index);
        }
    }

    if right_index < a.len() {
        heapify(a, right_index);
        if a[right_index] > a[root] {
            a.swap(right_index, root);
            sift_up(a, right_index);
        }
    }
}

pub fn sift_up<T>(a: &mut [T], root: usize)
        where T: PartialOrd + Debug
{
    println!("sift_up({:?}, {})", a, root);
    let left_index = left_child(root);
    let right_index = right_child(root);

    if left_index < a.len() && right_index < a.len() {
        if a[left_index] > a[right_index] {
            if a[left_index] > a[root] {
                a.swap(left_index, root);
                sift_up(a, left_index);
            }
        }
        else if a[right_index] > a[root] {
            a.swap(right_index, root);
            sift_up(a, right_index);
        }
    }
    else if left_index < a.len() {
        if a[left_index] > a[root] {
            a.swap(left_index, root);
            sift_up(a, left_index);
        }
    }
    else if right_index < a.len() {
        if a[right_index] > a[root] {
            a.swap(right_index, root);
            sift_up(a, right_index);
        }
    }
}

pub fn sort<T>(a: &mut [T])
        where T: PartialOrd + Debug
{
    heapify(a, 0);
    let length = a.len();
    for i in 0..length {
        a.swap(0, length - 1 - i);
        let mut sort_slice = &mut a[..length - 1 - i];
        sift_up(&mut sort_slice, 0);
    }
}

//Tests

#[test]
fn test_heapify() {
    let mut to_sort = [6, 5, 3, 1, 8, 7, 2, 4];
    let heapified = [8, 6, 7, 4, 5, 3, 2, 1];
    heapify(&mut to_sort, 0);
    assert_eq!(to_sort, heapified);
}

#[test]
fn test_sort() {
    let mut to_sort = [6, 5, 3, 1, 8, 7, 2, 4];
    let sorted = [1, 2, 3, 4, 5, 6, 7, 8];
    sort(&mut to_sort);
    assert_eq!(to_sort, sorted);
}
