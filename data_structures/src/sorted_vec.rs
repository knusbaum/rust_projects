use std::cmp::PartialOrd;
use std::fmt::Debug;

#[derive(Debug)]
pub struct SortedVec<T: PartialOrd + Debug> {
    pub elems: Vec<T>
}

//fn parent_index(i: usize) -> usize {
//    return (i - 1) / 2;
//}

fn left_child(i: usize) -> usize {
    return (2 * i) + 1;
}

fn right_child(i: usize) -> usize {
    return (2 * i) + 2;
}

impl<T: PartialOrd + Debug> SortedVec<T> {
    pub fn new() -> SortedVec<T> {
        SortedVec{elems: Vec::new()}
    }

    pub fn to_sorted_vec(elems: Vec<T>) -> SortedVec<T> {
        let mut vec = SortedVec{elems};
        vec.heapify(0);
        vec
    }

    fn heapify(&mut self, root: usize) {
        //println!("heapify ({}): {:?}", root, self.elems);
        let left_index = left_child(root);
        let right_index = right_child(root);

        if left_index < self.elems.len() {
            self.heapify(left_index);

            if self.elems[left_index] > self.elems[root] {
                self.elems.swap(left_index, root);
                SortedVec::sift_up(&mut self.elems[..], left_index);
            }
        }

        if right_index < self.elems.len() {
            self.heapify(right_index);
            if self.elems[right_index] > self.elems[root] {
                self.elems.swap(right_index, root);
                SortedVec::sift_up(&mut self.elems[..], right_index);
            }
        }
    }

    fn sift_up(vector: &mut [T], root: usize) {
        //println!("sift_up({:?}, {})", vector, root);
        let left_index = left_child(root);
        let right_index = right_child(root);

        if left_index < vector.len() && right_index < vector.len() {
            if vector[left_index] > vector[right_index] {
                if vector[left_index] > vector[root] {
                    vector.swap(left_index, root);
                    SortedVec::sift_up(vector, left_index);
                }
            }
            else if vector[right_index] > vector[root] {
                vector.swap(right_index, root);
                SortedVec::sift_up(vector, right_index);
            }
        }
        else if left_index < vector.len() {
            if vector[left_index] > vector[root] {
                vector.swap(left_index, root);
                SortedVec::sift_up(vector, left_index);
            }
        }
        else if right_index < vector.len() {
            if vector[right_index] > vector[root] {
                vector.swap(right_index, root);
                SortedVec::sift_up(vector, right_index);
            }
        }
    }

    pub fn sort(&mut self) {
        let length = self.elems.len();
        for i in 0..length {
            self.elems.swap(0, length - 1 - i);
            let mut sort_slice = &mut self.elems[..length - 1 - i];
            SortedVec::sift_up(&mut sort_slice, 0);
        }
    }

    pub fn get(&self, i: usize) -> Option<&T> {
        self.elems.get(i)
    }
}

//Tests

#[test]
fn test_sort() {
    let mut to_sort = SortedVec::to_sorted_vec(vec![6, 5, 3, 1, 8, 7, 2, 4]);
    let sorted = vec![1, 2, 3, 4, 5, 6, 7, 8];
    to_sort.sort();
    assert_eq!(to_sort.elems, sorted);
}
