use std::fmt;
use std::fmt::Display;
use std::fmt::Formatter;

#[derive(Debug)]
pub struct List<T> {
    head: Option<Box<ListNode<T>>>
}

#[derive(Debug)]
pub struct ListNode<T> {
    elem: T,
    next: Option<Box<ListNode<T>>>
}


impl<T> List<T> {
    pub fn new() -> List<T> {
        List{head: None}
    }

    pub fn push(&mut self, elem: T) {
        let new_node = ListNode{
            elem,
            next: self.head.take(),
        };
        self.head = Some(Box::new(new_node))
    }

    pub fn pop(&mut self) -> Option<T> {
        let head = self.head.take();
        match head {
            None => None,
            Some(box_node) => {
                self.head = box_node.next;
                Some(box_node.elem)
            }
        }
    }
}

impl<T: Display> Display for List<T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        
        write!(f, "List[")?;
        let mut current = &self.head;
        let mut first = true;
        loop {
            match current {
                None => return write!(f, "]"),
                Some(box_node) => {
                    if first {
                        write!(f, "{}", box_node.elem)?;
                        first = false;
                    } else {
                        write!(f, ", {}", box_node.elem)?;
                    }
                    current = &box_node.next;
                }
            }
        }
    }
}

// Iterators
pub struct IntoIter<T>(List<T>);

impl<T> List<T> {
    pub fn into_iter(self) -> IntoIter<T> {
        IntoIter(self)
    }
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        // access fields of a tuple struct numerically
        self.0.pop()
    }
}

pub struct Iter<'a, T> {
    next: Option<&'a ListNode<T>>,
}

impl<T> List<T> {
    pub fn iter(&self) -> Iter<T> {
        Iter{
            next: self.head.as_ref().map(|box_val| &**box_val )
        }
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        match self.next {
            None => return None,
            Some(node) => {
                self.next = node.next.as_ref().map(|node| &**node);
                return Some(&node.elem);
            }
        }
    }
}

pub struct IterMut<'a, T> {
    next: Option<&'a mut ListNode<T>>,
}

impl<T> List<T> {
    pub fn iter_mut(&mut self) -> IterMut<T> {
        IterMut{
            next: self.head.as_mut().map(|box_val| &mut **box_val )
        }
    }
}

impl<'a, T> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;
    fn next(&mut self) -> Option<Self::Item> {
        match self.next.take() {
            None => return None,
            Some(node) => {
                self.next = node.next.as_mut().map(|node| &mut **node);
                return Some(&mut node.elem);
            }
        }
    }
}

// Tests
#[test]
fn test_push_pop() {
    let mut lst: List<u32> = List::new();
    lst.push(0);
    lst.push(1);
    lst.push(2);
    lst.push(3);
    lst.push(4);
    assert_eq!(lst.pop().unwrap(), 4);
    assert_eq!(lst.pop().unwrap(), 3);
    assert_eq!(lst.pop().unwrap(), 2);
    assert_eq!(lst.pop().unwrap(), 1);
    assert_eq!(lst.pop().unwrap(), 0);
    assert_eq!(lst.pop(), None);
}

#[test]
fn into_iter() {
    let mut list: List<u32> = List::new();
    list.push(1); list.push(2); list.push(3);
    
    let mut iter = list.into_iter();
    assert_eq!(iter.next(), Some(3));
    assert_eq!(iter.next(), Some(2));
    assert_eq!(iter.next(), Some(1));
}

#[test]
fn iter() {
    let mut list: List<u32> = List::new();
    list.push(1); list.push(2); list.push(3);
    
    let mut iter = list.iter();
    assert_eq!(iter.next(), Some(&3));
    assert_eq!(iter.next(), Some(&2));
    assert_eq!(iter.next(), Some(&1));
}

#[test]
fn iter_mut() {
    let mut list = List::new();
    list.push(1); list.push(2); list.push(3);

    let mut iter = list.iter_mut();
    assert_eq!(iter.next(), Some(&mut 3));
    assert_eq!(iter.next(), Some(&mut 2));
    assert_eq!(iter.next(), Some(&mut 1));
}
