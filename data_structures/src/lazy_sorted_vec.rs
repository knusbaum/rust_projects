use std::cmp::PartialOrd;
use std::fmt::Debug;

#[derive(Debug)]
pub struct LazySortedVec<T: PartialOrd + Debug> {
    pub elems: Vec<T>,
    sort_start: usize
}

impl<T: PartialOrd + Debug> LazySortedVec<T> {
    pub fn new() -> LazySortedVec<T> {
        LazySortedVec{elems: Vec::new(), sort_start: 0}
    }

    pub fn to_lazy_sorted_vec(elems: Vec<T>) -> LazySortedVec<T> {
        let mut vec = LazySortedVec{elems, sort_start: 0};
        vec.heapify();
        vec
    }

    fn parent_index(&self, i: usize) -> usize {
        return (i + self.elems.len()) / 2;
    }

//    fn left_child(&self, i: usize) -> isize {
//        return ((2 * i) as isize) - self.elems.len() as isize - 1;
//    }

    fn right_child(&self, i: usize) -> isize {
        return ((2 * i) as isize) - self.elems.len() as isize;
    }

    fn heapify(&mut self) {
        let start = self.parent_index(self.elems.len() - 1);

        for x in 0..=start {
            self.sift_down(x);
        }
    }

    fn sift_down(&mut self, start: usize) {
        let mut root = start;

        let mut iright_index = self.right_child(root);
        while iright_index >= self.sort_start as isize {
            let right_index = iright_index as usize;

            let mut swapi = root;
            if self.elems[swapi] > self.elems[right_index] {
                swapi = right_index;
            }
            if iright_index - 1 >= self.sort_start as isize
                && self.elems[swapi] > self.elems[right_index - 1] {
                swapi = (right_index) - 1;
            }
            if swapi == root {
                return;
            }
            self.elems.swap(root, swapi);
            root = swapi;
            iright_index = self.right_child(root);
        }
    }

    pub fn sort(&mut self) {
        while self.sort_start < self.elems.len() {
            let length = self.elems.len();
            self.elems.swap(self.sort_start, length - 1);
            self.sort_start = self.sort_start + 1;
            self.sift_down(length - 1);
        }
    }

    pub fn element(&mut self, i: usize) -> Option<&T> {
        if i < self.elems.len() {
            let length = self.elems.len();
            while self.sort_start <= i {
                self.elems.swap(self.sort_start, length - 1);
                self.sort_start += 1;
                self.sift_down(length - 1);
            }
            return Some(&self.elems[i]);
        }
        return None;
    }
}

pub struct IntoIter<T: PartialOrd + Debug>(LazySortedVec<T>);

impl<T: PartialOrd + Debug> LazySortedVec<T> {
    pub fn into_iter(self) -> IntoIter<T> {
        IntoIter(self)
    }
}

impl<T: PartialOrd + Debug> Iterator for IntoIter<T> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        match self.0.element(0) {
            None => None,
            Some(_) => {
                let ret = self.0.elems.remove(0);
                self.0.sort_start = 0;
                return Some(ret);
            }
        }
    }
}

pub struct Iter<'a, T: PartialOrd + Debug> {
    vec: &'a mut LazySortedVec<T>,
    i: usize
}

impl<T: PartialOrd + Debug> LazySortedVec<T> {
    pub fn iter(&mut self) -> Iter<T> {
        Iter{
            vec: self,
            i: 0
        }
    }
}

impl<'a, T: 'a + PartialOrd + Debug> Iterator for Iter<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        println!("self.i: {} self.vec.elems.len(): {}", self.i, self.vec.elems.len());
        if self.i >= self.vec.elems.len() {
            return None;
        }

        match self.vec.element(self.i) {
            None => return None,
            Some(elem) => {
                self.i = self.i + 1;
                let uret: *const T = elem;
                unsafe {
                    return Some(&*uret);
                }
            }
        }
    }
}

pub struct IterMut<'a, T: PartialOrd + Debug> {
    vec: &'a mut LazySortedVec<T>,
    i: usize
}

impl<T: PartialOrd + Debug> LazySortedVec<T> {
    pub fn iter_mut(&mut self) -> IterMut<T> {
        IterMut{
            vec: self,
            i: 0
        }
    }
}

impl<'a, T: 'a + PartialOrd + Debug> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;
    fn next(&mut self) -> Option<Self::Item> {
        println!("self.i: {} self.vec.elems.len(): {}", self.i, self.vec.elems.len());
        if self.i >= self.vec.elems.len() {
            return None;
        }

        match self.vec.element(self.i) {
            None => return None,
            Some(_) => {
                let uret: *mut T = &mut self.vec.elems[self.i];
                self.i = self.i + 1;
                unsafe {
                    return Some(&mut *uret);
                }
            }
        }
    }
}

// Tests
#[test]
fn test_element() {
    let mut vec = LazySortedVec::to_lazy_sorted_vec(vec![6, 5, 3, 1, 8, 7, 2, 4]);
    let sorted = vec![1, 2, 3, 4, 5, 6, 7, 8];

    for i in 0..sorted.len() {
        assert_eq!(sorted[i], *vec.element(i).unwrap());
    }
}

#[test]
fn into_iter() {
    let vec = LazySortedVec::to_lazy_sorted_vec(vec![3, 1, 2, 4]);
    let mut iter = vec.into_iter();
    assert_eq!(iter.next(), Some(1));
    assert_eq!(iter.next(), Some(2));
    assert_eq!(iter.next(), Some(3));
    assert_eq!(iter.next(), Some(4));
    assert_eq!(iter.next(), None);
}

#[test]
fn iter() {
    let mut vec = LazySortedVec::to_lazy_sorted_vec(vec![3, 1, 2, 4]);
    let mut iter = vec.iter();
    assert_eq!(iter.next(), Some(&1));
    assert_eq!(iter.next(), Some(&2));
    assert_eq!(iter.next(), Some(&3));
    assert_eq!(iter.next(), Some(&4));
    assert_eq!(iter.next(), None);
}

#[test]
fn iter_mut() {
    let mut vec = LazySortedVec::to_lazy_sorted_vec(vec![3, 1, 2, 4]);
    let mut iter = vec.iter_mut();
    assert_eq!(iter.next(), Some(&mut 1));
    assert_eq!(iter.next(), Some(&mut 2));
    assert_eq!(iter.next(), Some(&mut 3));
    assert_eq!(iter.next(), Some(&mut 4));
    assert_eq!(iter.next(), None);
}
