use std::fmt::Debug;

pub struct RingBuffer<T> {
    elems: Vec<T>,
    zero_index: usize,
}

impl<T: Debug> RingBuffer<T> {
    pub fn new(size: usize) -> RingBuffer<T> {
        RingBuffer{
            elems: Vec::with_capacity(size),
            zero_index: 0
                
        }
    }

    pub fn add(&mut self, elem: T) {
        if self.zero_index >= self.elems.len() && self.zero_index < self.elems.capacity() {
            self.elems.push(elem);
            self.zero_index = (self.zero_index + 1) % self.elems.capacity();
        }
        else {
            self.elems[self.zero_index] = elem;
            self.zero_index = (self.zero_index + 1) % self.elems.capacity();
        }
    }
    pub fn elem(&self, i: usize) -> Option<&T> {
        if i > self.elems.len() {
            return None;
        }
        if self.elems.len() < self.elems.capacity() {
            // We've not filled the buffer yet. First elem is at 0
            return Some(&self.elems[i]);
        }
        let index = (self.zero_index + i) % self.elems.len();
        return Some(&self.elems[index]);
        
    }
}

// Iterator
pub struct Iter<'a, T>{
    buf: &'a RingBuffer<T>,
    i: usize
}

impl<T: Debug> RingBuffer<T> {
    pub fn iter(&self) -> Iter<T> {
        Iter{
            buf: self,
            i: 0
        }
    }
}

impl<'a, T: Debug> Iterator for Iter<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        if self.i >= self.buf.elems.len() {
            return None;
        }
        let ret = self.buf.elem(self.i);
        self.i += 1;
        return ret;
    }
}

#[test]
fn new_buffer() {
    let mut buf: RingBuffer<u32> = RingBuffer::new(5);
    
    buf.add(1);
    assert_eq!(buf.elem(0), Some(&1));
    
    buf.add(2);
    assert_eq!(buf.elem(0), Some(&1));
    assert_eq!(buf.elem(1), Some(&2));
    
    buf.add(3);
    assert_eq!(buf.elem(0), Some(&1));
    assert_eq!(buf.elem(1), Some(&2));
    assert_eq!(buf.elem(2), Some(&3));
    
    buf.add(4);
    assert_eq!(buf.elem(0), Some(&1));
    assert_eq!(buf.elem(1), Some(&2));
    assert_eq!(buf.elem(2), Some(&3));
    assert_eq!(buf.elem(3), Some(&4));
    
    buf.add(5);
    assert_eq!(buf.elem(0), Some(&1));
    assert_eq!(buf.elem(1), Some(&2));
    assert_eq!(buf.elem(2), Some(&3));
    assert_eq!(buf.elem(3), Some(&4));
    assert_eq!(buf.elem(4), Some(&5));
    
    buf.add(6);
    assert_eq!(buf.elem(0), Some(&2));
    assert_eq!(buf.elem(1), Some(&3));
    assert_eq!(buf.elem(2), Some(&4));
    assert_eq!(buf.elem(3), Some(&5));
    assert_eq!(buf.elem(4), Some(&6));
    
    buf.add(7);
    assert_eq!(buf.elem(0), Some(&3));
    assert_eq!(buf.elem(1), Some(&4));
    assert_eq!(buf.elem(2), Some(&5));
    assert_eq!(buf.elem(3), Some(&6));
    assert_eq!(buf.elem(4), Some(&7));
    
    buf.add(8);
    assert_eq!(buf.elem(0), Some(&4));
    assert_eq!(buf.elem(1), Some(&5));
    assert_eq!(buf.elem(2), Some(&6));
    assert_eq!(buf.elem(3), Some(&7));
    assert_eq!(buf.elem(4), Some(&8));
    
}

#[test]
fn iter() {
    let mut buf: RingBuffer<u32> = RingBuffer::new(5);
    buf.add(1);
    buf.add(2);
    buf.add(3);
    buf.add(4);
    buf.add(5);

    let expected = [1, 2, 3, 4, 5];
    let mut i = 0;
    for e in buf.iter() {
        assert_eq!(e, &expected[i]);
        i += 1;
    }

    buf.add(6);
    buf.add(7);
    buf.add(8);

    let expected = [4, 5, 6, 7, 8];
    let mut i = 0;
    for e in buf.iter() {
        assert_eq!(e, &expected[i]);
        i += 1;
    }
}
