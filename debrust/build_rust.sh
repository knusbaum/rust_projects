#!/bin/bash

set -x

#echo "ALL ARGS: $@"

dir=$1
shift

cd "$dir"
cargo clean
cargo build $@ 

#echo $dir
#echo rest $@
