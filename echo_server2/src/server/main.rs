extern crate echo_server2;

use echo_server2::messages::ToServerMessage;
use echo_server2::messages::ToClientMessage;
use echo_server2::messages::Privmsg;
use serde_json::Deserializer;
use serde_json;
use std::io::Write;
use std::io::{BufReader, BufWriter};
use std::io;
use std::net::{TcpListener, TcpStream};
use std::sync::mpsc::Receiver;
use std::sync::mpsc::Sender;
use std::sync::mpsc;
use std::thread;

#[derive(Debug)]
enum ClientMessage {
    Send(String),
    FromClient(ToServerMessage)
}

#[derive(Debug)]
enum RouterMessage {
    NewClient(CHandle),
    Broadcast(String)
}

struct ClientHandler {
    writer: BufWriter<TcpStream>,
    mailbox: Option<Receiver<ClientMessage>>,
    mailbox_sender: Sender<ClientMessage>,
    router: Sender<RouterMessage>
}

impl ClientHandler {

    fn write_privmsg(&mut self, target: &str, msg: &str) -> Result<(),std::io::Error> {
        serde_json::to_writer(
            &mut self.writer,
            &ToClientMessage::PRIVMSG(
                Privmsg{
                    target: target.to_string(),
                    msg: msg.to_string()
                }
            ))?;
        self.writer.flush()?;
        Ok(())
    }

    fn write_prompt(&mut self, prompt: &str) -> Result<(), std::io::Error> {
        serde_json::to_writer(
            &mut self.writer,
            &ToClientMessage::ServerPrompt(prompt.to_string())
        )?;
        self.writer.flush()?;
        Ok(())
    }

    fn startup(&mut self) -> Result<String, std::io::Error> {
        self.write_privmsg("T", "What's your name?")?;
        self.write_prompt("Your Name: ")?;
        loop {
            match self.mailbox {
                Some(ref mailbox) => {
                    let msg = mailbox.recv();
                    if let Err(msg) = msg {
                        return Err(std::io::Error::new(std::io::ErrorKind::Other, format!("{}", msg)));
                    }
                    match msg.unwrap() {
                        ClientMessage::FromClient(msg) => {
                            let ToServerMessage::PRIVMSG(msg) = msg;
                            return Ok(msg.msg);
                        },
                        ClientMessage::Send(_) => {}
                    }
                },
                None => {
                    return Err(std::io::Error::new(std::io::ErrorKind::Other, format!("{}", "No Mailbox.")));
                }
            }
        }
    }

    fn run(&mut self) {
        let mut nick = "".to_string();
        match self.startup() {
            Ok(n) => {
                nick = n;
            },
            Err(e) => {
                println!("Startup error: {}", e);
            }
        }

        let result = self.router.send(RouterMessage::NewClient(
            CHandle{
                sender: self.mailbox_sender.clone(),
                nick
            }
        ));
        // If we can't send to the router, we should shut down.
        if let Err(e) = result {
            println!("Failed to send to router: {}", e);
            return;
        }


        let mailbox = self.mailbox.take().unwrap();
        for msg in mailbox.iter() {
            match msg {
                ClientMessage::Send(s) => {
                    println!("Sending: {}", s);
                    if let Err(e) = self.write_privmsg("YOU", &s) {
                        println!("Failed to send message to client: {}", e);
                        return;
                    }
                },
                ClientMessage::FromClient(msg) => {
                    println!("Got Message from client.");
                    match msg {
                        ToServerMessage::PRIVMSG(msg) => {
                            let result = self.router.send(RouterMessage::Broadcast(msg.msg));
                            if let Err(e) = result {
                                println!("ClientHandler can't talk to router. Killing client: {}", e);
                                return;
                            }
                        }
                    }
                }
            }
        }
        self.mailbox = Some(mailbox);
    }
}

#[derive(Debug)]
struct CHandle {
    sender: Sender<ClientMessage>,
    nick: String
}

struct Router {
    clients: Vec<CHandle>,
    mailbox: Receiver<RouterMessage>
}

impl Router {
    fn run(&mut self) {
        for msg in self.mailbox.iter() {
            match msg {
                RouterMessage::NewClient(client_chan) => {
                    println!("New Client: {:?}", client_chan);
                    self.clients.push(client_chan);
                },
                RouterMessage::Broadcast(s) => {
                    let mut i = 0;
                    while i != self.clients.len() {
                        match self.clients[i].sender.send(ClientMessage::Send(s.clone())) {
                            Ok(_) => {i+=1;},
                            Err(e) => {
                                println!("Failed to send to client: {}", e);
                                self.clients.remove(i);
                            }
                        }
                    }
                    // Unstable API
//                    self.clients.drain_filter(|c| {
//                        println!("Broadcasting to {:?}", c);
//                        match c.send(ClientMessage::Send(s.clone())) {
//                            Ok(_) => {false},
//                            Err(e) => {
//                                println!("Failed to send to client.");
//                                true
//                            }
//                        }
//                    });
                }
            }
        }
    }
}

fn main() -> io::Result<()> {
    let listener = TcpListener::bind("0.0.0.0:60000")?;
    let (router_sender, router_receiver) = mpsc::channel();

    let mut router = Router{
        clients: vec![],
        mailbox: router_receiver
    };

    thread::spawn(move || {
        router.run();
        println!("Router exiting.");
    });

//    let repeater = router_sender.clone();
//    thread::spawn(move || {
//        loop {
//            repeater.send(RouterMessage::Broadcast("test".to_string()));
//            thread::sleep(Duration::new(0, 100000000));
//        }
//    });

    for stream_result in listener.incoming() {
        match stream_result {
            Err(e) => {
                println!("Failed to listen: {}", e);
            },
            Ok(stream) => {
                let (client_sender, client_receiver) = mpsc::channel();
                handle_accept(stream, router_sender.clone(), client_receiver, client_sender.clone());
            }
        }
    }
    Ok(())
}

fn handle_accept(stream: TcpStream, router: Sender<RouterMessage>, mailbox: Receiver<ClientMessage>, client_sender: Sender<ClientMessage>) {
    println!("Got client: {:?}", stream);
    //Router::spawn_client_handler(stream, &client_tx);
    match stream.try_clone() {
        Ok(clone) => {
            //let reader = BufReader::new(clone);
            let reader: BufReader<TcpStream> = BufReader::new(clone);
            let writer: BufWriter<TcpStream> = BufWriter::new(stream);
            let mut client = ClientHandler{
                writer,
                mailbox: Some(mailbox),
                mailbox_sender: client_sender.clone(),
                router
            };
            thread::spawn(move || {
                client.run();
                println!("Client thread exiting.");
            });
            thread::spawn(move || {
                let stream = Deserializer::from_reader(reader).into_iter::<ToServerMessage>();
                for msg in stream {
                    match msg {
                        Ok(msg) => {
                            client_sender.send(ClientMessage::FromClient(msg)).ok();
                        },
                        Err(e) => {
                            println!("Failed to send server message to client: {}", e);
                            return;
                        }
                    }
                }
                println!("Client reader exiting.");
            });
        },
        Err(e) => {
            println!("Failed to clone TcpStream: {}", e);
        }
    }
}
