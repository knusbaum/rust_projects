//use serde::{Deserialize, Serialize};
//use serde_json::Result;

#[derive(Debug, Serialize, Deserialize)]
pub struct Privmsg {
    pub target: String,
    pub msg: String
}

#[derive(Debug, Serialize, Deserialize)]
pub enum ToClientMessage {
    PRIVMSG(Privmsg),
    ServerPrompt(String)
//    OTHER(String),
}

#[derive(Debug, Serialize, Deserialize)]
pub enum ToServerMessage {
    PRIVMSG(Privmsg)
}
