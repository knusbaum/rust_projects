extern crate echo_server2;

use echo_server2::messages::ToServerMessage;
use echo_server2::messages::ToClientMessage;
use echo_server2::messages::Privmsg;
use ncurses::*;
use serde_json::Deserializer;
use std::io::Write;
use std::io::{BufReader, BufWriter};
use std::net::{TcpStream};
use std::panic;
use std::sync::mpsc::Receiver;
use std::sync::mpsc::Sender;
use std::sync::mpsc;
use std::thread;

struct CommandLine {
    window: WINDOW,
    val: String,
    option_str: String,
    prompt_str: String
}

impl CommandLine {

    fn commands() -> Vec<String> {
        vec!["/history".to_string(),
             "/nick".to_string()]
    }

    fn starts_command(s: &str) -> Vec<String> {
        let mut fitting = CommandLine::commands();
        let mut i = 0;
        while i != fitting.len() {
            if fitting[i].starts_with(s) {
                i += 1;
            }
            else {
                fitting.remove(i);
            }
        }
        return fitting;
    }

    fn redraw(&self) {
        wclear(self.window);
        wmove(self.window, 0, 0);
        waddstr(self.window, &self.prompt_str);
        waddstr(self.window, &self.val);
        waddch(self.window, '\t' as u32);
        waddstr(self.window, &self.option_str);
        wrefresh(self.window);
    }

    fn update(&mut self, ch: i32) {
        if self.val.len() > 0 && self.val.as_bytes()[0] as char == '/' && ch == '\t' as i32 {
            let commands = CommandLine::starts_command(&self.val);
            self.option_str = format!("{:?}", commands);
        }
        else {
            self.option_str.clear();
        }
        if ch < 127 && ch > 31 {
            // character is in printable range.
            self.val.push((ch as u8) as char);
        } else if ch == 127 {
            // Backspace
            self.val.pop();
        }
        self.redraw();
    }
}

#[derive(Debug)]
enum IFMessage {
    ClientInput(i32),
    ServerMessage(ToClientMessage),
    Raw(String),
    ServerQuit(String)
}

struct ClientInterface {
    cline: CommandLine,
    border_window: WINDOW,
    main_window: WINDOW,
    mailbox: Option<Receiver<IFMessage>>,
    server: BufWriter<TcpStream>
}

impl ClientInterface {

    fn recreate_windows(&mut self) {
        let mainwin = stdscr();
        let maxx = getmaxx(mainwin);
        let maxy = getmaxy(mainwin);

        wresize(self.cline.window, 1, maxx);
        mvwin(self.cline.window, maxy-1, 0);

        wresize(self.main_window, maxy-3, maxx-3);
        mvwin(self.main_window, 1, 1);

        wresize(self.border_window, maxy-1, maxx-1);
        mvwin(self.border_window, 0, 0);
        wborder(self.border_window,
                '|' as u32,
                '|' as u32,
                '-' as u32,
                '-' as u32,
                '+' as u32,
                '+' as u32,
                '+' as u32,
                '+' as u32);

        wrefresh(self.border_window);
        wrefresh(self.main_window);
        self.cline.redraw();
    }

    fn setup(if_receiver: Receiver<IFMessage>, server_sender: BufWriter<TcpStream>) -> ClientInterface {
        let mainwin = stdscr();
        let maxx = getmaxx(mainwin);
        let maxy = getmaxy(mainwin);

        let commandline = newwin(1, maxx-1, maxy-1, 0);
        let main_window = newwin(maxy-3, maxx-3, 1, 1);
        let border_window = newwin(maxy-1, maxx-1, 0, 0);
        wborder(border_window,
                '|' as u32,
                '|' as u32,
                '-' as u32,
                '-' as u32,
                '+' as u32,
                '+' as u32,
                '+' as u32,
                '+' as u32);
        wrefresh(border_window);
        scrollok(main_window, true);
        ClientInterface {
            cline: CommandLine {
                window: commandline,
                val: "".to_string(),
                option_str: "".to_string(),
                prompt_str: "".to_string()
            },
            border_window: border_window,
            main_window: main_window,
            mailbox: Some(if_receiver),
            server: server_sender
        }
    }

    fn write_main_line(&self, s: &str) {
        let mut x: i32 = 0;
        let mut y: i32 = 0;
        getyx(self.main_window, &mut y, &mut x);
        if x > 0 {
            waddstr(self.main_window, "\n");
        }
        waddstr(self.main_window, s);
    }

    fn handle_client_input(&mut self, ch: i32) -> bool {
        if ch <= 127 && ch != 4 {
            self.cline.update(ch);
        }
        if ch == '\n' as i32 {
            // return key
            self.cline.prompt_str.clear();
            let result = serde_json::to_writer(
                &mut self.server,
                &ToServerMessage::PRIVMSG(
                    Privmsg{
                        target: "YOU".to_string(),
                        msg: self.cline.val.clone()
                    }
                ));
            if let Err(e) = result {
                eprintln!("Failed to serialize message to server: {}", e);
                return true;
            }
            self.cline.val.clear();
            self.cline.redraw();
            if let Err(e) = self.server.flush() {
                eprintln!("Failed to write to server: {}", e);
                return true;
            }
        }
        else if ch == KEY_RESIZE {
            self.recreate_windows();
        }
        else if ch == 4 {
            // EOF
            return true;
        }
        return false;
    }

    fn interface_main_loop(&mut self) {
        let mut i = 0;
        let mailbox = self.mailbox.take().unwrap();
        for msg in mailbox.iter() {

            match msg {
                IFMessage::ClientInput(ch) => {
                    if self.handle_client_input(ch) {
                        eprintln!("Client decided to quit.");
                        return;
                    }
                },
                IFMessage::ServerMessage(msg) => {
                    match msg {
                        ToClientMessage::PRIVMSG(msg) => {
                            self.write_main_line(&format!("{} target: {}, msg: {}", i, msg.target, msg.msg));
                            i += 1;
                        },
                        ToClientMessage::ServerPrompt(msg) => {
                            self.cline.prompt_str = msg;
                            self.cline.redraw();
                        }
                    }
                    wrefresh(self.main_window);
                },
                IFMessage::Raw(s) => {
                    self.write_main_line(&s);
                    wrefresh(self.main_window);
                },
                IFMessage::ServerQuit(msg) => {
                    eprintln!("Server quit: {}", msg);
                    return;
                }
            }
        }
        self.mailbox = Some(mailbox);
    }
}

fn user_input_loop(if_sender: Sender<IFMessage>) {
    loop {
        let ncurses_char = ncurses::getch();
        let result = if_sender.send(IFMessage::ClientInput(ncurses_char));
        if let Err(_) = result {
            // Just shut down this thread if we can't talk to the interface.
            return;
        }
    }
}

fn read_from_server_loop(if_sender: Sender<IFMessage>, stream: BufReader<TcpStream>) {
    let stream = Deserializer::from_reader(stream).into_iter::<ToClientMessage>();
    for msg in stream {
        match msg {
            Ok(msg) => {
                let send_result = if_sender.send(IFMessage::ServerMessage(msg));
                if let Err(e) = send_result {
                    eprintln!("Server reader Failed to talk to client interface: {}", e);
                    return;
                }
            },
            Err(e) => {
                let send_result = if_sender.send(IFMessage::ServerQuit(format!("Error reading from server: {}", e)));
                if let Err(e) = send_result {
                    eprintln!("Server reader Failed to talk to client interface: {}", e);
                    return;
                }
            }
        }
    }
}

fn main() {
    /* If your locale env is unicode, you should use `setlocale`. */
    // let locale_conf = LcCategory::all;
    // setlocale(locale_conf, "zh_CN.UTF-8"); // if your locale is like mine(zh_CN.UTF-8).

    /* Start ncurses. */

    ncurses::initscr();
    raw();
    noecho();
    keypad(stdscr(), true);

    /* Update the screen. */
    ncurses::refresh();

    curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);

    panic::set_hook(Box::new(|p| {
        eprintln!("PANIC: {}", p);
        ncurses::endwin();
    }));

    let (if_sender, if_receiver) = mpsc::channel();

    let stream = TcpStream::connect("127.0.0.1:60000");
    match stream {
        Err(e) => {
            // We can ignore this send failure. The server connection is defunct anyway.
            if_sender.send(IFMessage::Raw(format!("Failed to connect: {}", e))).ok();
            ncurses::endwin();
            eprintln!("Failed to connect: {}", e);
            return;
        },
        Ok(stream) => {
            match stream.try_clone() {
                Ok(clone) => {
                    let stream_reader = BufReader::new(clone);
                    let stream_writer = BufWriter::new(stream);

                    let server_sender = if_sender.clone();
                    thread::spawn(move || {
                        read_from_server_loop(server_sender, stream_reader);
                    });

                    let if_thread = thread::spawn(move || {
                        let mut interface = ClientInterface::setup(if_receiver, stream_writer);
                        interface.interface_main_loop();
                    });

                    let client_input_sender = if_sender.clone();
                    thread::spawn(move || {
                        user_input_loop(client_input_sender);
                    });
                    if_thread.join().ok();
                },
                Err(e) => {
                    ncurses::endwin();
                    eprintln!("Failed to connect: {}", e);
                    return;
                }
            }
        }
    }

    /* Terminate ncurses. */
    ncurses::endwin();
}
