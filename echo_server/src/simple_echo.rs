use std::net::{TcpListener, TcpStream};
use std::io;
use std::io::Read;

fn received_message(s: &str) {
    print!("{}", s);
}

fn handle_client(mut stream: TcpStream) {
    let mut buffer = [0; 1024];

    loop {
        //println!("READING!");
        match stream.read(&mut buffer) {
            Err(e) => {
                println!("Failed to read from TcpStream: {}", e);
                return;
            },
            Ok(size) => {
                //println!("Read {}.", size);
                if size == 0 {
                    println!("Remote closed connection.");
                    return;
                }
                match std::str::from_utf8(&buffer[..size]) {
                    Err(e) => {
                        println!("Don't understand the encoding: {}", e);
                        return;
                    },
                    Ok(s) => {
                        //print!("{}", s);
                        received_message(s);
                    }
                }
            }
        }
    }
}

fn main() -> io::Result<()> {
    let listener = TcpListener::bind("localhost:60000")?;
    for stream_result in listener.incoming() {
        match stream_result {
            Err(e) => {
                println!("Failed to listen: {}", e);
                return Err(e);
            },
            Ok(stream) => {
                handle_client(stream);
            }
        }
    }
    return Ok(());
}
