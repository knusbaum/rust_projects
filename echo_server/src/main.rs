use std::net::{TcpListener, TcpStream, Shutdown};
use std::io;
use std::io::{BufReader, BufWriter, BufRead};
use std::io::Write;
use std::thread;
use std::sync::mpsc;
use data_structures::ring_buffer::RingBuffer;
use std::collections::HashMap;
use uuid::Uuid;

mod command;

#[derive(Debug)]
struct Client {
    id: String,
    name: String,
    writer: BufWriter<TcpStream>
}

fn handle_client(mut reader: BufReader<TcpStream>, mut writer: BufWriter<TcpStream>, channel: mpsc::Sender<Message>) {
    // Initialize by getting client name.
    if let Err(e) = writer.write(b"Enter your name: ") {
        println!("Failed to send to client: {}", e);
        return;
    }
    if let Err(e) = writer.flush() {
        println!("Failed to send to client: {}", e);
        return;
    }

    let mut name = String::new();
    if let Err(e) = reader.read_line(&mut name) {
        println!("Failed to get name from client: {}", e);
        return;
    }

    let name = name.trim();
    let id = Uuid::new_v4();

    // Send the write channel to the router.
    let msg = Message::MsgWriter(Client{
        id: id.to_string(),
        name: name.to_string(),
        writer: writer
    });

    if let Err(e) = channel.send(msg) {
        println!("Failed to send to router: {}", e);
        return;
    }

    // Listen to the client.
    loop {
        let mut line = String::new();
        match reader.read_line(&mut line) {
            Err(e) => {
                println!("Failed to read from TcpStream: {}", e);
                break;
            },
            Ok(size) => {
                if size == 0 {
                    println!("Remote closed connection.");
                    break;
                }
                let line = line.trim();
                if line.starts_with('/') {
                    // User issued a command.
                    let command = &line[1..];
                    if let Err(e) = channel.send(Message::MsgComm(id.to_string(), command.to_string())) {
                        println!("Failed to send to router: {}", e);
                        return;
                    }
                }
                else {
                    if let Err(e) = channel.send(Message::MsgStr(id.to_string(), line.to_string())) {
                        println!("Failed to send to router: {}", e);
                        return;
                    }
                }
            }
        }
    }
    println!("Client {} thread dying.", id);
}

#[derive(Debug)]
enum Message {
    MsgStr(String, String),
    MsgComm(String, String),
    MsgWriter(Client)
}

type CommandHandler = fn(router: &mut Router, client_id: &str, args: &[&str]);

struct Router {
    clients: HashMap<String, Client>,
    history: RingBuffer<String>,
    command_handlers: HashMap<String, CommandHandler>
}

impl Router {
    fn new() -> Router {
        Router {
            clients: HashMap::new(),
            history: RingBuffer::new(20),
            command_handlers: HashMap::new()
        }
    }

    pub fn add_command_handler(&mut self, name: &str, handler: CommandHandler) {
        println!("Adding command handler for [{}]", name);
        self.command_handlers.insert(name.to_string(), handler);
        println!("Have handlers:");
        for h in self.command_handlers.keys() {
            println!("HAVE: [{}]", h);
        }
    }

    fn spawn_client_handler(stream: TcpStream, client_tx: &mpsc::Sender<Message>) {
        match stream.try_clone() {
            Ok(clone) => {
                let reader = BufReader::new(clone);
                let writer = BufWriter::new(stream);

                let tx_clone = mpsc::Sender::clone(&client_tx);
                thread::spawn(move || {
                    handle_client(reader, writer, tx_clone);
                });
            },
            Err(e) => {
                println!("Failed to clone TcpStream: {}", e);
            }
        }
    }

    pub fn run(&mut self, host_port: &str) -> io::Result<()> {
        let listener = TcpListener::bind(host_port)?;

        let (client_tx, client_rx) = mpsc::channel();
        thread::spawn(move || {
            for stream_result in listener.incoming() {
                match stream_result {
                    Err(e) => {
                        println!("Failed to listen: {}", e);
                    },
                    Ok(stream) => {
                        Router::spawn_client_handler(stream, &client_tx);
                    }
                }
            }
        });

        self.message_router(client_rx);
        return Ok(());
    }

    fn kill_client(&mut self, id: &str) {
        if let Some(mut client) = self.clients.remove(id) {
            client.writer.get_mut().shutdown(Shutdown::Both).ok();
            self.broadcast(&format!("{} left...\n", client.name));
        }
    }

    fn send_history(&mut self, id: &str) {
        if let Some(client) = self.clients.get_mut(id) {
            client.writer.write(b"History: ####################\n").ok();
            for msg in self.history.iter() {
                client.writer.write_fmt(format_args!("{}", msg)).ok();
            }
            client.writer.write(b"#############################\n").ok();
            client.writer.flush().ok();
        }
    }

    fn broadcast(&mut self, s: &str) {
        let mut removals: Vec<String> = vec![];
        for client in self.clients.values_mut() {
            client.writer.write_fmt(format_args!("{}",s)).ok();
            if let Err(e) = client.writer.flush() {
                println!("Failed to write: {}. Removing client: {}({})",
                         e, client.id, client.name);
                removals.push(client.id.clone());
            }
        }
        for removal in removals.iter() {
            self.kill_client(removal);
        }
        self.history.add(s.to_string());
    }

    fn handle_new_writer(&mut self, mut client: Client) {
        // Write room members and history
        client.writer.write(b"Room members: \n").ok();
        for c in self.clients.values() {
            client.writer.write_fmt(format_args!("\t{}\n", c.name)).ok();
        }
        match client.writer.flush() {
            Ok(_) => {
                let msg = &format!("{} Joined...\n", client.name);
                self.broadcast(msg);
                let id = client.id.clone();
                self.clients.insert(id.clone(), client);
                self.send_history(&id);
            },
            Err(e) => {
                println!("Failed to write: {}. Removing client: {}({})",
                         e, client.id, client.name);
                client.writer.get_mut().shutdown(Shutdown::Both).ok();
            }
        }
    }

    fn handle_command(&mut self, id: &str, command: &str) {
        let parts: Vec<_> = command.split_whitespace().collect();
        let command = parts[0];

        match self.command_handlers.get(&command.to_string()) {
            Some(handler) => {
                if let Some(client) = self.clients.get(id) {
                    println!("Client {}({}) issued command: {}", client.id, client.name, command);
                }
                handler(self, id, &parts[1..]);
            },
            None => {
                if let Some(client) = self.clients.get_mut(id) {
                    println!("Client {}({}) issued unknown command: {}", client.id, client.name, parts[0]);
                    client.writer.write_fmt(format_args!("Don't understand command {}\n", command)).ok();
                    client.writer.flush().ok();
                }
            }
        }
    }

    pub fn message_router(&mut self, channel: mpsc::Receiver<Message>) {
        for msg in channel.iter() {
            println!("Routing message: {:?}", msg);
            match msg {
                Message::MsgStr(id, s) => {
                    let client = &self.clients[&id];
                    let message = format!("{}: {}\n", client.name, &s);
                    self.broadcast(&message);
                },
                Message::MsgWriter(client) => {
                    self.handle_new_writer(client);
                }
                Message::MsgComm(id, command) => {
                    self.handle_command(&id, &command);
                }
            }
        }
    }
}

fn quit_command(router: &mut Router, client_id: &str, _args: &[&str]) {
    router.kill_client(&client_id);
}

fn history_command(router: &mut Router, client_id: &str, _args: &[&str]) {
    router.send_history(&client_id);
}

fn nick_command(router: &mut Router, client_id: &str, args: &[&str]) {
    if args.len() == 1 {
        if let Some(client) = router.clients.get(client_id) {
            router.broadcast(&format!("{} is now known as {}\n", client.name, args[0]));
        }
        if let Some(client) = router.clients.get_mut(client_id) {
            client.name = args[0].to_string();
        }
    }
}

fn main() -> io::Result<()> {
    let mut router = Router::new();
    router.add_command_handler("quit", quit_command);
    router.add_command_handler("history", history_command);
    router.add_command_handler("nick", nick_command);
    router.run("localhost:60000")
}
